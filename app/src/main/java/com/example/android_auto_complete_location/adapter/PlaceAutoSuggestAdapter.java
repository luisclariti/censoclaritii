package com.example.android_auto_complete_location.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.android_auto_complete_location.CallesEntry;
import com.example.android_auto_complete_location.LawyersDbHelper;
import com.example.android_auto_complete_location.models.PlaceApi;
import com.example.android_auto_complete_location.sql.ConnectionHelper;
import com.google.gson.Gson;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static com.example.android_auto_complete_location.CallesEntry.CallesEntryBd.NombreCalle;

public class PlaceAutoSuggestAdapter extends ArrayAdapter implements Filterable {

    ArrayList<String> results;

    int resource;
    Context context;

    PlaceApi placeApi=new PlaceApi();

    public PlaceAutoSuggestAdapter(Context context,int resId){
        super(context,resId);
        this.context=context;
        this.resource=resId;

    }

    @Override
    public int getCount(){
        return results.size();
    }

    @Override
    public String getItem(int pos){
        return results.get(pos);
    }

    @Override
    public Filter getFilter(){
        Filter filter=new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults=new FilterResults();
                if(constraint!=null){


                    results=placeApi.autoComplete(constraint.toString());

                    filterResults.values=results;

                    filterResults.count=results.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results!=null && results.count>0){
                    notifyDataSetChanged();
                }
                else{
                    notifyDataSetInvalidated();
                }

            }
        };
        return filter;
    }

}
