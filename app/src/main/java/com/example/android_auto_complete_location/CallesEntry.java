package com.example.android_auto_complete_location;

import android.provider.BaseColumns;

public class CallesEntry {

    public static abstract class CallesEntryBd implements BaseColumns {
        public static final String TABLE_NAME ="Calles";

        public static final String id =       "id";
        public static final String NombreCalle =       "NombreCalle";

    }
}
