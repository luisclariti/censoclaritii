package com.example.android_auto_complete_location;

import android.provider.BaseColumns;

public class Datacenso {


    public static abstract class LawyerEntry implements BaseColumns {
        public static final String TABLE_NAME ="DataCenso";

        public static final String CUENTA =       "cuenta";
        public static final String NUMERO =       "numero";
        public static final String GIRO =         "giro";
        public static final String CALLE =        "calle";
        public static final String ENTRECALLE =   "entrecalle";
        public static final String ENTRECALLE2 =  "entrecalle2";
        public static final String FOTO =         "foto";
        public static final String CELULAR =      "celular";
        public static final String UBICACION =    "ubicacion";
        public static final String UBICACION2 =   "ubicacion2";
        public static final String UBICACION3 =   "ubicacion3";
        public static final String UBIMED =       "ubimed";
    }
}
