package com.example.android_auto_complete_location.models;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.example.android_auto_complete_location.CallesEntry;
import com.example.android_auto_complete_location.Datacenso;
import com.example.android_auto_complete_location.LawyersDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;


import static com.example.android_auto_complete_location.CallesEntry.CallesEntryBd.NombreCalle;

public class PlaceApi {

    public ArrayList<String> autoComplete(String input){
        ArrayList<String> arrayList=new ArrayList();
        HttpURLConnection connection=null;
        StringBuilder jsonResult=new StringBuilder();




        try {
          // StringBuilder sb=new StringBuilder("https://maps.googleapis.com/maps/api/place/autocomplete/json?");
            StringBuilder sb=new StringBuilder("http://j49er.mocklab.io/calleinegi");
            //sb.append("input="+input);
            //sb.append("&key=AIzaSyDPjBO7dyz-JaxksSPeKBQL98cI7TQj5Dc");
            URL url=new URL(sb.toString());
            connection=(HttpURLConnection)url.openConnection();


            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            InputStreamReader inputStreamReader=new InputStreamReader(connection.getInputStream());

            SQLiteDatabase db;

            int read;

            char[] buff=new char[1024];
            while ((read=inputStreamReader.read(buff))!=-1){
                jsonResult.append(buff,0,read);
            }
        }
        catch (MalformedURLException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            if(connection!=null){
                connection.disconnect();
            }
        }

        try {


            JSONObject jsonObject=new JSONObject(jsonResult.toString());
            JSONArray prediction=jsonObject.getJSONArray("data");
            for(int i=0;i<prediction.length();i++){
                arrayList.add(prediction.getJSONObject(i).getString("NombreCalle"));



            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }


        return arrayList;
    }


}
