package com.example.android_auto_complete_location;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;

import static android.content.ContentValues.TAG;

public class LawyersDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DataCenso.db";



    public LawyersDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        //super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        super(context, "DataCenso.db", factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + Datacenso.LawyerEntry.TABLE_NAME + " ("
                + Datacenso.LawyerEntry.CUENTA + " TEXT,"
                + Datacenso.LawyerEntry.NUMERO + " TEXT,"
                + Datacenso.LawyerEntry.GIRO + " TEXT,"
                + Datacenso.LawyerEntry.CALLE + " TEXT,"
                + Datacenso.LawyerEntry.ENTRECALLE + " TEXT,"
                + Datacenso.LawyerEntry.ENTRECALLE2 + " TEXT,"
                + Datacenso.LawyerEntry.FOTO + " TEXT,"
                + Datacenso.LawyerEntry.CELULAR + " TEXT,"
                + Datacenso.LawyerEntry.UBICACION + " TEXT,"
                + Datacenso.LawyerEntry.UBICACION2 + " TEXT,"
                + Datacenso.LawyerEntry.UBICACION3 + " TEXT,"
                + Datacenso.LawyerEntry.UBIMED + " TEXT,"
                + Datacenso.LawyerEntry.UBIMED + "TEXT)");



        sqLiteDatabase.execSQL("CREATE TABLE " + CallesEntry.CallesEntryBd.TABLE_NAME + " ("
                + CallesEntry.CallesEntryBd.id + ","
                + CallesEntry.CallesEntryBd.NombreCalle + " TEXT,"
                + CallesEntry.CallesEntryBd.NombreCalle + "TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
