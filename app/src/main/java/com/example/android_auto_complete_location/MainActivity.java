package com.example.android_auto_complete_location;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android_auto_complete_location.adapter.PlaceAutoSuggestAdapter;
import com.example.android_auto_complete_location.librerias.AsyncRequestInterface;
import com.example.android_auto_complete_location.librerias.Response;
import com.example.android_auto_complete_location.sql.ConnectionHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.example.android_auto_complete_location.CallesEntry.CallesEntryBd.NombreCalle;
import static com.example.android_auto_complete_location.CallesEntry.CallesEntryBd.id;

@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {


    GoogleMap map;
    private ImageButton scanBtn;
    private Button guardarBtn;
    private EditText editTextCuenta,textViewNum,textViewGiro,textViewCalle, textViewentrec, textViewentrec2, editTextCel;
    private Spinner spinner, spinner2;
    private ImageButton imageButton;

    private GoogleMap mMap;




    String[] users = {"Frente", "Derecha", "Izquierda", "Atras" };
    String[] users2 = {"Habitada", "Des-Habitada" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        AutoCompleteTextView autoCompleteTextView=findViewById(R.id.autocomplete);
        autoCompleteTextView.setAdapter(new PlaceAutoSuggestAdapter(MainActivity.this,android.R.layout.simple_list_item_1));



        Spinner spin = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);

        Spinner spin2 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, users2);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin2.setAdapter(adapter2);



       ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
    //    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 1);



        scanBtn = (ImageButton)findViewById(R.id.scanbtn);
        editTextCuenta = (EditText) findViewById(R.id.editTextCuenta);
        textViewNum = (EditText) findViewById(R.id.textViewNum);
        textViewGiro = (EditText) findViewById(R.id.textViewGiro);
        textViewCalle = (EditText) findViewById(R.id.autocomplete);
        textViewentrec = (EditText) findViewById(R.id.autocomplete2);
        textViewentrec2 = (EditText) findViewById(R.id.autocomplete3);
        spinner =  findViewById(R.id.spinner);
        editTextCel = (EditText) findViewById(R.id.editTextCel);
        imageButton = (ImageButton) findViewById(R.id.imageButton);
        editTextCel = (EditText) findViewById(R.id.editTextCel);
        guardarBtn = (Button) findViewById(R.id.guardar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        scanBtn.setOnClickListener(this);

        imageButton.setOnClickListener(this);


    }



    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        Toast.makeText(getApplicationContext(), "Selected User: "+users[position] ,Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO - Custom Code
    }



    public void onClick(View v){
        if(v.getId()==R.id.scanbtn){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 1);
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();


        }
        if(v.getId()==R.id.imageButton){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 1);
            openCameraIntent();
        }
    }

    private static final int REQUEST_CAPTURE_IMAGE = 100;

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE
        );
        if(pictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(pictureIntent,
                    REQUEST_CAPTURE_IMAGE);
        }
    }


    public void alta(View v) {
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        float gps = location.getAccuracy();
        String gpssave = String.valueOf(gps);

        ConnectionHelper con = new ConnectionHelper();
        Connection connect = ConnectionHelper.CONN();

        LawyersDbHelper admin = new LawyersDbHelper(this,"DataCenso", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String cuenta = editTextCuenta.getText().toString();
        String numero = textViewNum.getText().toString();
        String giro = textViewGiro.getText().toString();
        String calle = textViewCalle.getText().toString();
        String entrecalle = textViewentrec.getText().toString();
        String entrecalle2 = textViewentrec2.getText().toString();
        String foto = imageButton.toString();
        String celular = editTextCel.getText().toString();
        String ubicacion = gpssave;
        String ubicacion2 = gpssave;
        String ubicacion3 = gpssave;
        String ubimed = spinner.getSelectedItem().toString();
        ContentValues DataCenso = new ContentValues();
        DataCenso.put("cuenta", cuenta);
        DataCenso.put("numero", numero);
        DataCenso.put("giro", giro);
        DataCenso.put("calle", calle);
        DataCenso.put("entrecalle", entrecalle);
        DataCenso.put("entrecalle2", entrecalle2);
        DataCenso.put("foto", foto);
        DataCenso.put("celular", celular);
        DataCenso.put("ubicacion", ubicacion);
        DataCenso.put("ubicacion2", ubicacion2);
        DataCenso.put("ubicacion3", ubicacion3);
        DataCenso.put("ubimed", ubimed);
        bd.insert("DataCenso", null, DataCenso);
        bd.close();
        editTextCuenta.setText("");
        textViewNum.setText("");
        textViewGiro.setText("");
        textViewCalle.setText("");
        textViewentrec.setText("");
        textViewentrec2.setText("");
        //imageButton.set
        editTextCel.setText("");
        editTextCuenta.setText("");
        editTextCuenta.setText("");
        editTextCuenta.setText("");
        editTextCuenta.setText("");
        Toast.makeText(this, "Se guardaron los datos del hogar",
                Toast.LENGTH_SHORT).show();
    }


    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    static final int REQUEST_TAKE_PHOTO = 1;




    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();

            editTextCuenta.setText( scanContent);
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (requestCode == REQUEST_CAPTURE_IMAGE &&
                resultCode == RESULT_OK) {
            if (intent != null && intent.getExtras() != null) {
                Bitmap imageBitmap = (Bitmap) intent.getExtras().get("data");
                imageButton.setImageBitmap(imageBitmap);
            }
        }
    }


    public boolean checkLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1 /* El codigo que puse a mi request */: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // aqui ya tengo permisos
                    Toast.makeText(MainActivity.this,
                            " Permiso listo",
                            Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // aqui no tengo permisos
                    Toast.makeText(MainActivity.this,
                            " Permission Denied",
                            Toast.LENGTH_SHORT)
                            .show();

                }
                return;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //checkLocationPermission();
        MarkerOptions mp = new MarkerOptions();


        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);

        double longitude = location.getLongitude();
        double latitude = location.getLatitude();

                    //-107.3926461728034
        //24.8099127462844365
         LatLng Maharashtra = new LatLng(latitude,longitude);
       // LatLng Maharashtra = new LatLng(longitude,latitude);
       map.addMarker(new MarkerOptions().position(Maharashtra).title("Ubicacion"));
       map.moveCamera(CameraUpdateFactory.newLatLng(Maharashtra));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(Maharashtra,15));


    }



}
