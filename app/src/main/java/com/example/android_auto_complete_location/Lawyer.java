package com.example.android_auto_complete_location;

public class Lawyer {
    private String cuenta;
    private String numero;
    private String calle;
    private String entrecalle;
    private String entrecalle2;
    private String foto;
    private String celular;
    private String ubicacion;
    private String ubicacion2;
    private String ubicacion3;
    private String ubimed;

    public Lawyer(String cuenta,
                  String numero, String calle,
                  String entrecalle, String entrecalle2, String foto, String celular, String ubicacion, String ubicacion2, String ubicacion3, String ubimed) {
        this.cuenta = cuenta;
        this.numero = numero;
        this.calle = calle;
        this.entrecalle = entrecalle;
        this.entrecalle2 = entrecalle2;
        this.entrecalle = foto;
        this.entrecalle2 = celular;
        this.entrecalle = ubicacion;
        this.entrecalle2 = ubicacion2;
        this.entrecalle = ubicacion3;
        this.entrecalle2 = ubimed;
    }

    public String getCuenta() {
        return cuenta;
    }

    public String getNumero() {
        return numero;
    }

    public String getCalle() {
        return calle;
    }

    public String getEntrecalle() {
        return entrecalle;
    }

    public String getEntrecalle2() {
        return entrecalle2;
    }

    public String getFoto() {
        return foto;
    }

    public String getCelular() {
        return celular;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getUbicacion2() {
        return ubicacion2;
    }

    public String getUbicacion3() {
        return ubicacion3;
    }

    public String getUbimed() {
        return ubimed;
    }
}