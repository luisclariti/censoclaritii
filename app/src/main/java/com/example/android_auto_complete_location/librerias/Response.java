package com.example.android_auto_complete_location.librerias;


import org.json.JSONArray;



public class Response {
    public static final String TAG = "Response";
    public static final byte SESSION = 0;
    public static final byte ERROR = 1;
    public static final byte INFO = 2;
    public static final byte WARNING = 3;
    public static final byte SUCCESS = 4;
    public static final byte PERMISSION = 5;
    public static final byte NO_MESSAGE = 6;
    public static final byte TIMEOUT = 7;

    private boolean valid;
    private String message;
    private int type;
    private boolean messages;
    private JSONArray data;

    public Response() {
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessageType() {
        return type;
    }

    public void setMessageType(int type) {
        this.type = type;
    }

    public boolean hasMessages() {
        return messages;
    }

    public void setHasMessages(boolean messages) {
        this.messages = messages;
    }

    public JSONArray getData() {
        return data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }

}
