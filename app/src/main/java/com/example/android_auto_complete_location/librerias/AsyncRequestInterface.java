package com.example.android_auto_complete_location.librerias;

import com.google.android.gms.common.api.Response;

public interface AsyncRequestInterface {

	public void onFinish(Response result);
	
}
